project.allprojects {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

tasks.register<Task>(name = "clean") {
    delete(rootProject.buildDir)
}