package ru.mts.teta.eliseev.feature1

class Feature1 {

    fun printName() {
        println("Current feature name ${this::class.simpleName}")
    }
}