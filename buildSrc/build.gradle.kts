plugins {
    `kotlin-dsl`
}

repositories {
    google()
    mavenCentral()
    gradlePluginPortal()
}

gradlePlugin {
    plugins {
        register("android-app-plugin") {
            id = "android-app-plugin"
            implementationClass = "ru.mts.teta.eliseev.gradle.plugin.AndroidAppPlugin"
        }
        register("android-library-plugin") {
            id = "android-library-plugin"
            implementationClass = "ru.mts.teta.eliseev.gradle.plugin.AndroidLibraryPlugin"
        }
        register("printer-plugin") {
            id = "printer-plugin"
            implementationClass = "ru.mts.teta.eliseev.gradle.plugin.PrinterPlugin"
        }
    }
}

dependencies {
    implementation("com.android.tools.build:gradle:7.2.2")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.10")
    implementation("com.github.lalyos:jfiglet:0.0.9")
}
