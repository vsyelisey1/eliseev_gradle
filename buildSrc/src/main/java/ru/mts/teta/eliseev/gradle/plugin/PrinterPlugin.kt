package ru.mts.teta.eliseev.gradle.plugin

import com.github.lalyos.jfiglet.FigletFont
import org.gradle.api.Plugin
import org.gradle.api.Project

class PrinterPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        val figletTask = target.tasks.register("figlet") {
            group = "myTasks"
            doLast {
                println(FigletFont.convertOneLine("MTS TETA Eliseev Gradle/CI/CD"))
            }
        }

        target.tasks.findByName("assemble")?.finalizedBy(figletTask)
    }
}
