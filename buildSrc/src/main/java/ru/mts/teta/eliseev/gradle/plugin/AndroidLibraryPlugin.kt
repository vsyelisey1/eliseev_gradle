package ru.mts.teta.eliseev.gradle.plugin

import com.android.build.gradle.LibraryExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.apply
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

class AndroidLibraryPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target.project) {
            apply(plugin = "com.android.library")
            apply(plugin = "kotlin-android")
        }

        target.extensions.getByType(LibraryExtension::class.java).apply {
            compileSdkVersion(AndroidAppConfig.COMPILE_SDK)

            defaultConfig {
                minSdk = AndroidAppConfig.MIN_SDK
                targetSdk = AndroidAppConfig.TARGET_SDK
            }

            configBuildTypes()
            configCompileOptions()
            configKotlin()
        }
    }

    private fun LibraryExtension.configCompileOptions() {
        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
    }

    private fun LibraryExtension.configBuildTypes() {
        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
            }
        }
    }

    private fun LibraryExtension.configKotlin() {
        val action: KotlinJvmOptions.() -> Unit = { jvmTarget = "1.8" }

        (this as ExtensionAware).extensions.configure("kotlinOptions", action)
    }
}