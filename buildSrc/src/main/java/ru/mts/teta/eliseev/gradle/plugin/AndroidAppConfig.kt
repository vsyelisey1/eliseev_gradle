package ru.mts.teta.eliseev.gradle.plugin

object AndroidAppConfig {
    const val APP_ID = "ru.mts.teta.eliseev.gradle"
    const val MIN_SDK = 21
    const val TARGET_SDK = 32
    const val COMPILE_SDK = 32
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0.0"
}
