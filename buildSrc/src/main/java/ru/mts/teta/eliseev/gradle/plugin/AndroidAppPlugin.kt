package ru.mts.teta.eliseev.gradle.plugin

import com.android.build.gradle.AppExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.apply
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

class AndroidAppPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target.project) {
            apply(plugin = "com.android.application")
            apply(plugin = "kotlin-android")
        }

        target.extensions.getByType(AppExtension::class.java).apply {
            compileSdkVersion(AndroidAppConfig.COMPILE_SDK)

            defaultConfig {
                applicationId = AndroidAppConfig.APP_ID
                minSdk = AndroidAppConfig.MIN_SDK
                targetSdk = AndroidAppConfig.TARGET_SDK
                versionCode = AndroidAppConfig.VERSION_CODE
                versionName = AndroidAppConfig.VERSION_NAME
            }

            configBuildTypes()
            configCompileOptions()
            configKotlin()
        }
    }

    private fun AppExtension.configCompileOptions() {
        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
    }

    private fun AppExtension.configBuildTypes() {
        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
            }
        }
    }

    private fun AppExtension.configKotlin() {
        val action: KotlinJvmOptions.() -> Unit = { jvmTarget = "1.8" }

        (this as ExtensionAware).extensions.configure("kotlinOptions", action)
    }
}
