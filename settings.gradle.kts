pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "MtsTetaGradle"
include(":app")
include(":feature1")
include(":feature2")
