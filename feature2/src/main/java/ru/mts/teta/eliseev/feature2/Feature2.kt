package ru.mts.teta.eliseev.feature2

class Feature2 {

    fun printName() {
        println("Current feature name ${this::class.simpleName}")
    }
}