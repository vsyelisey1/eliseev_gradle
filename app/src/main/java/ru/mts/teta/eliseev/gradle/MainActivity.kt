package ru.mts.teta.eliseev.gradle

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import ru.mts.teta.eliseev.feature1.Feature1
import ru.mts.teta.eliseev.feature2.Feature2

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Feature1().printName()
        Feature2().printName()
        Speaker().sayHello()
    }
}