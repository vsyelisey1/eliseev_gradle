plugins {
    id("android-app-plugin")
    id("printer-plugin")
}

sourceSets {
    forEach { srcSet ->
        srcSet.java {
            srcDirs("${project.buildDir.path}/codegen")
        }
    }
}

tasks.create<DefaultTask>(name = "generateSpeaker") {
    doLast {
        project.layout.projectDirectory.dir("src/main/java/ru/mts/teta/eliseev/gradle").apply {
            asFile.mkdirs()
            file("Speaker.kt").asFile.writeText(
                """
                package ru.mts.teta.eliseev.gradle
                    
                open class Speaker {
                
                    fun sayHello() {
                        println("Hello MTS Teta!")
                    }
                }
                """.trimIndent()
            )
        }
    }
}

tasks.getByName("preBuild") {
    dependsOn.add(tasks.getByName("generateSpeaker"))
}

dependencies {
    implementation(project(":feature1"))
    implementation(project(":feature2"))

    implementation("androidx.core:core-ktx:1.8.0")
    implementation("androidx.appcompat:appcompat:1.5.0")
    implementation("com.google.android.material:material:1.6.1")
}